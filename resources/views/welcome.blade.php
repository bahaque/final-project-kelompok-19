@extends('layouts.app')

@section('content')
<div class="container">
    @if (Route::has('login'))
        <div>
            @auth
                @include('newsfeed')            
            @else
                <div>
                    <h2>Sanbercode</h2>
                    <p>
                        Selamat Datang!
                    </p>
                </div>
                
                <div>
                    <strong>Already a member? <a href="{{ route('login') }}">Login</a></strong>
                </div>

                <div>
                    @if (Route::has('register'))
                        <strong>New?<a href="{{ route('register') }}">Register</a></strong>
                    @endif
                </div>
            @endauth
        </div>
    @endif
</div>
@endsection